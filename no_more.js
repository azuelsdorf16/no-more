// Copyright (c) 2017 Andrew Zuelsdorf. All rights reserved.
 
const badWords = ["Breitbart", "Jorge Ramos", "Romney", "Christie", "Bannon", "Maher", "Kushner", "Obama", "Lahren", "Steve King", "Kim Weaver", "Trevor Noah", "Tapper", "Dana Bash", "Maddow", "Spicer", "Roy Wood, Jr", "Klepper", "Putin ", "Jeff Sessions", "Rahm Emmanuel", "Eric Holder", "Jill Stein", "Gary Johnson", "Tucker Carlson", "Bloomberg", "Huma Abedin", "Anthony Weiner", "John Edwards", "Alex Jones", "Seth Meyers", "Sen. Sanders", "Senator Sanders", "Elizabeth Warren", "Pence", "Paul Ryan", "Boehner", "Mitch McConnell", "Marco Rubio", "Ted Cruz", "Mnuchin", "Liz Warren", "George W. Bush", "Reagan", "Jon Oliver", "Jon Stewart", "Megan Kelly", "Roger Ailes", "Bill O'Reilly", "Colbert", "Justin Trudeau", "Nigel Farage", "Boris Johnson", "Sharpton", "Richard Spencer", "O'Malley", "Omarosa", "Manigault", "Ben Carson", "Kellyanne Conway", "Cecily Strong", "Alec Baldwin", "Kate McKinnon", "Michael Cohen", "Stormy Daniels", "Roseanne Barr", "Bill Maher", "Samantha Bee" , "Jon Oliver", "Rudy Giuliani", "Mitt Romney", "Michelle Wolf", "Michael Wolff", "Ivanka", "Eric Trump", "Donald Trump", "Hillary Clinton", "Trump", "Giuliani", "Stephanopoulos", "Mueller", "Baldwin", "Pompeo", "Farage", "Romney", "Wolff", "Sen. Warren", "Senator Warren"];

var askToBlock = true;
var blockEnabled = true;
var debug = false;

function containsCensoredWords(innerHtml) {
    if (!innerHtml) {
        return '';
    }

    for (var i = 0; i < badWords.length; i += 1) {
        if (innerHtml.indexOf(badWords[i].toUpperCase()) !== -1) {
            return badWords[i];
        }
    }
    return '';
}

function relevantKids(kids) {
    if (!kids) {
        return 0;
    }
    
    var numRelevant = 0;
    
    for (var i = 0; i < kids.length; i += 1) {
        if (kids[i].tagName !== "A" &&
                kids[i].tagName !== "STRONG" && kids[i].tagName !== "I") {
            numRelevant += 1;
        }
    }
    
    return numRelevant;
}

function blockFromBody() {
    if (!blockEnabled) {
        return;
    }

    var stack = [];

    var body = document.getElementsByTagName("*");

    for (var i = 0; i < body.length; i += 1) {
        var element1 = body[i];

        if (relevantKids(element1.children) === 0 && element1.innerHTML != null) {
            var badWord = containsCensoredWords(element1.innerHTML.toUpperCase());
            if (badWord !== "") {
                if (askToBlock) {
                    askToBlock = false;
                    var retVal = window.confirm("Warning! Censorable content \"" + badWord +
                                                "\" appeared on this page. Do you want to " + 
                                                "block it?");

                    if (!retVal) {
                        blockEnabled = false;
                        return;
                    }
                }

                element1.innerHTML = "Warning! Dangerous content!";
            }
        }
    }
}

//Retreives the fully qualified domain name from a URL.
function extractKey(url) {
    var domain = null;

    if (url.indexOf("://") > -1) {
        domain = url.split("/")[2];
    }
    else {
        domain = url.split("/")[0];
    }

    domain = domain.split(":")[0];

    if (domain.indexOf("/") > -1) {
        domain = domain.split("/")[0];
    }

    return domain;
}

function isNewsSite(locationHref) {
    var newsSites = ["cnn.com", "foxnews.com", "theepochtimes.com",
"epochtimes.com", "breitbart.com", "bbc.com", "bbc.co.uk", "msnbc.com",
"cbs.com", "abcnews.go.com", "buzzfeed.com", "nytimes.com",
"washingtonpost.com", "latimes.com", "msn.com", "wired.com",
"arstechnica.com", "techrepublic.com", "nbc.com", "youtube.com"];

    for (var i = 0; i < newsSites.length; ++i) {
        if (locationHref.endsWith(newsSites[i])) {
            return true;
        }
    }
}

function main() {
    if (isNewsSite(extractKey(window.location.href))) {
        if (debug) {
            alert("\"" + extractKey(window.location.href) +
"\" is a news site.");
        }

        window.setInterval(blockFromBody, 1000);
    }
    else {
        if (debug) {
            alert("\"" + extractKey(window.location.href) +
"\" is not a news site.");
	}
    }
}

main();
